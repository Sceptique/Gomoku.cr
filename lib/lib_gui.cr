@[Include(
  "lib/gui/gui.h",
  prefix: %w(gui_),
  remove_prefix: true,
)]
@[Link("gui")]
lib LibGui
end
