# NAME=`ls src/*/ -d | cut -f2 -d'/'`

all: deps_opt client

client: gui
	crystal build src/Client/Client.cr --stats

crystal_lib:
	[ -d crystal_lib ] || git clone https://github.com/crystal-lang/crystal_lib

gui: crystal_lib
	gcc -c -Wall -Werror -fpic lib/gui/gui.c
	gcc -shared -o libgui.so lib/gui/gui.o
	crystal crystal_lib/src/main.cr -- lib/lib_gui.cr > src/LibGomoku.cr

# run:
# 	crystal run src/$(NAME).cr
# build:
# 	crystal build src/$(NAME).cr --stats
# release:
# 	crystal build src/$(NAME).cr --stats --release
test:
	crystal spec
deps:
	crystal deps install
deps_update:
	crystal deps update
deps_opt:
	@[ -d lib/ ] || make deps
doc:
	crystal docs

.PHONY: all run build release test deps deps_update doc gui client server
